
import copy
import os
import time

from drl.unpublished.ddpg1step import Ddpg1Step
from drl.shared_code.processing import batch_to_tensors
from drl.shared_code.exploration import GaussianNoise
import numpy as np
import torch
from torch.utils.data.dataset import random_split
from torch.utils.data import DataLoader

from supervised_learning import Dataset


class PretrainOffPolicyMixin():
    def init_pretrain(self, pretrain_samples=1000, pretrain_epochs=20,
                      retrain_interval=None, random_init_actions=False, 
                      store_init_samples=True, supervised_batch_size=None,
                      supervised_lr=0.001, train_share=0.8, 
                      train_over_time=False,
                      *args, **kwargs):

        self.train_over_time = train_over_time
        self.pretrain_samples = pretrain_samples
        self.pretrain_epochs = pretrain_epochs
        self.retrain_interval = retrain_interval
        self.random_init_actions = random_init_actions
        self.store_init_samples = store_init_samples
        self.supervised_batch_size = (
            supervised_batch_size if supervised_batch_size 
            else min(self.batch_size, pretrain_samples))
        self.supervised_lr = supervised_lr
        self.train_share = train_share
        self.critic_is_trained = False

        if not store_init_samples:
            self.opt_memory = self.memory.__class__(
                pretrain_samples, self.n_obs, self.n_act, n_rewards=self.n_rewards)

        self.model = copy.deepcopy(self.actor)
        self.pretrain_optimizer = torch.optim.Adam(
            self.model.parameters(), lr=self.supervised_lr)
        if pretrain_samples:
            self.collect_opt_data()
            if not train_over_time:
                self.full_pretrain()
            else:
                # Distribute supervised pretraining over the whole time until 
                # self.start_train (the reinforcement learning)
                self.total_train_steps = pretrain_epochs * pretrain_samples * train_share / self.supervised_batch_size
                self.supervised_train_counter = 0
                self.supervised_learning_step()

    def collect_opt_data(self):
        """ Collect optimal data for supervised training and later replay. """
        env_space = self.env.unwrapped.action_space
        agent_space = self.env.action_space
        memory = self.memory if self.store_init_samples else self.opt_memory
        start = time.time()
        while len(memory) < self.pretrain_samples:
            obs, info = self.env.reset()
            old_reward = self.env.unwrapped.baseline_reward()
            if np.isnan(old_reward):
                continue
            env_act = self.env.unwrapped.get_current_actions()

            # Re-scale env action to action space of the agent
            act = ((agent_space.high - agent_space.low) *
                   (env_act - env_space.low) /
                   (env_space.high - env_space.low) + agent_space.low)

            # Apply action again to aquire all data (e.g next obs)
            next_obs, reward, terminated, truncated, info = self.env.step(act)

            # Store optimal result to replay buffer to re-use them later
            memory.store_transition(obs, act, reward, next_obs, terminated)
            
        self.env_time = time.time() - start

        # Use all the data as a single batch
        batch = memory.sample_random_batch(len(memory))
        self.opt_batch = batch_to_tensors(batch, self.device, continuous=True)

        opt_data = Dataset(*self.opt_batch[:2])
        n_train = int(self.train_share * len(opt_data))
        opt_train_data, opt_val_data = random_split(
            opt_data, [n_train, len(opt_data) - n_train])
        self.opt_train_data = DataLoader(opt_train_data, batch_size=self.supervised_batch_size, shuffle=True)
        self.opt_val_data = DataLoader(opt_val_data, batch_size=self.pretrain_samples)
        
    def full_pretrain(self):
        """ Perform supervised training until convergence. """
        start = time.time()
        best_loss = np.inf
        losses = []
        for epoch in range(self.pretrain_epochs):
            print('Supervised learning epoch: ', epoch)
            self.supervised_learning_epoch()
            losses.append(self.validation_test())
            print('validation loss: ', losses[-1])
            # Only really store model if performance improved
            if losses[-1] < best_loss:
                best_loss = losses[-1]
                self.actor = copy.deepcopy(self.model)
                print('New best model stored')
        self.train_time = time.time() - start   
             
        # Store train losses to csv
        file_path = os.path.join(self.path, 'pretrain_validation_losses.csv')
        np.savetxt(file_path, losses) 

        # Update target nets to new state
        self.actor_target = copy.deepcopy(self.actor)

        self.start_time = time.time()
        # Test initial performance without any RL
        self.test(test_steps=40)

    def supervised_learning_step(self):
        """ Sample a single batch and perform a single training step. """
        print('perform one pretrained learning step')
        obs, opt_act = next(iter(self.opt_train_data))
        self.supervised_learning(obs, opt_act)
        # TODO: How to prevent overfitting here? Is it even a problem?
        self.actor = self.model
        self.supervised_train_counter += 1

    def supervised_learning_epoch(self):
        for obs, opt_act in self.opt_train_data:
            self.supervised_learning(obs, opt_act)

    def supervised_learning(self, obs, opt_acts):
        self.model.train()
        self.pretrain_optimizer.zero_grad()
        act = self.model(obs)
        loss = torch.nn.functional.mse_loss(act, opt_acts)
        loss.backward()
        self.pretrain_optimizer.step()

    def validation_test(self):
        """ Compute the loss on the supervised validation dataset. """
        self.model.eval()
        # Load the whole validation dataset at once
        obs, opt_act = next(iter(self.opt_val_data))
        act = self.model(obs)
        loss = (act - opt_act).abs().mean()
        return loss.item()
    
    def learn(self, *args, **kwargs):
        """ Add pretraining over time to the training process. """
        if self.train_over_time and len(self.memory) < self.start_train:
            # Pretrain for n steps
            train_counter_should = (self.step / self.start_train) * self.total_train_steps
            while self.supervised_train_counter < train_counter_should:
                self.supervised_learning_step()

        elif len(self.memory) >= self.start_train and not self.critic_is_trained:
            # Perform all critic train steps en block before the actor training
            # starts. This is the latest point before actor training starts
            # with the biggest dataset available.
            print('Perform critic "pretraining" before actor training starts')
            self.critic_is_trained = True

            # TODO: Since this is supervised learning, we could also use a 
            # validation set to check for overfitting here 
            for _ in range(self.step * self.train_steps // self.train_interval):
                batch = self.memory.sample_random_batch(self.batch_size)
                batch = batch_to_tensors(batch, self.device, continuous=True)
                self._train_critic(*batch)
                self._soft_target_update(self.critic, self.critic_target, self.tau)

        super().learn(*args, **kwargs)
    

class PretrainDdpg(PretrainOffPolicyMixin, Ddpg1Step):
    def __init__(self, env, init_noise_std=None, 
                 *args, **kwargs):
        super().__init__(env, *args, **kwargs)

        # Special noise to use only in the beginning because actor is static in 
        # that time, which results in too little exploration
        if init_noise_std:
            self.init_noise = GaussianNoise((self.n_act,), init_noise_std)
        else:
            self.init_noise = self.noise

        self.init_pretrain(*args, **kwargs)

    def act(self, obs):
        """ Use actor to create actions and add noise for exploration. """
        if (len(self.memory) < self.start_train and self.random_init_actions):
            return self.env.action_space.sample()
        elif len(self.memory) < self.start_train:
            # Use special higher noise in the beginning for exploration
            action = self.test_act(obs) + self.init_noise()
        else:
            action = self.test_act(obs) + self.noise()
        return np.clip(action, self.min_range, 1)

    def _learn(self, *args, **kwargs):
        if self.retrain_interval and self.step % self.retrain_interval == 0:
            self.supervised_learning(*next(iter(self.opt_train_data)))
        super()._learn(*args, **kwargs)
