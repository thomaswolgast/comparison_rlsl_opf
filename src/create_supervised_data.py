import argparse
import csv
from datetime import datetime
import json
import os
from time import time

import numpy as np
from gymnasium import spaces
from drl.wrappers.obs_scaler import ObsScaler
import mlopf.envs as envs

from utils import rescale_action


def main():
    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        '--path',
        type=str,
        help="Directory of the data folder",
        default="training_data/"
    )
    argparser.add_argument(
        '--name',
        type=str,
        help="Name of the dataset/experiment",
        default="",
    )
    argparser.add_argument(
        '--dataset-size',
        type=int,
        help="How many samples to generate",
        default=10000,
    )
    argparser.add_argument(
        '--environment',
        type=str,
        help="Name of the environment class from the mlopf package",
        default="QMarket",
    )

    args = argparser.parse_args()

    # Load environment from mlopf package
    env_class = getattr(envs, args.environment)
    print('Create dataset for', env_class.__name__, 'environment')
    env = ObsScaler(env_class(), -1, 1)

    path = args.path if args.path.endswith('/') else args.path + '/'
    name = datetime.now().isoformat(timespec='seconds') if not args.name else args.name
    path = f'{path}{name}_{args.environment}_{args.dataset_size}/'
    os.makedirs(os.path.dirname(path), exist_ok=True)

    original_action_space = env.unwrapped.action_space
    # Assumption: Neural net has an tanh output
    target_action_space = spaces.Box(-1, 1, shape=original_action_space.shape)
    start = time()
    counter = 0
    inputs = []
    targets = []
    opt_rewards = []
    non_converged = 0
    while counter < args.dataset_size:
        print(f'{counter}/{args.dataset_size}')

        observation, info = env.reset(options={'test': False})

        optimal_reward = env.baseline_reward()
        if np.isnan(optimal_reward):
            non_converged += 1
            continue

        inputs.append(observation)
        opt_rewards.append((optimal_reward, ))

        action = env.get_current_actions()
        action = rescale_action(action, original_action_space, target_action_space)
        targets.append(action)

        counter += 1

        if counter % 100 == 0:
            print('Writing data...')
            write_data(path, inputs, targets, opt_rewards)
            inputs, targets, opt_rewards = [], [], []

    write_data(path, inputs, targets, opt_rewards)

    execution_time = time() - start

    # Write results to JSON
    results = {"dataset_size": args.dataset_size,
                "environment": args.environment,
                "execution_time": execution_time,
                "non_converged": non_converged}
    with open(path + "meta_data.json", "w") as f:
        json.dump(results, f, indent=4)


def write_data(path, inputs, targets, opt_rewards):
    with open(path + "inputs.csv", "a") as f:
        csv.writer(f).writerows(inputs)

    with open(path + "targets.csv", "a") as f:
        csv.writer(f).writerows(targets)

    with open(path + "opt_rewards.csv", "a") as f:
        csv.writer(f).writerows(opt_rewards)


if __name__ == '__main__':
    main()
