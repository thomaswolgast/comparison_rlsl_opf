""" Generate data for metrics calculation. Compares a single RL trained model 
with a single SL trained model. """

import argparse
import copy
from datetime import datetime
import json
import os
from time import time

import gymnasium as gym
import matplotlib.pyplot as plt
import pandas as pd

from drl.agent import ScalerObs
from mlopf import envs
from mlopf.envs.thesis_envs import EcoDispatchEnv
import mlopf.envs.thesis_envs as envs 
from supervised_learning import initialize_model
from utils import *


DEVICE = ('cuda:0' if torch.cuda.is_available() else 'cpu')

# Plot settings
FIG_SIZE = (10, 7)
FONT_SIZE = 11

def main():
    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        '--description',
        type=str,
        help="Description of the experiment",
        default="",
    )
    argparser.add_argument(
        '--data-path',
        type=str,
        help="Directory of the data folder (usually automatically set from sl-path)",
        default="",
    )
    argparser.add_argument(
        '--results-path',
        type=str,
        help="Directory of the results folder",
        default="results/"
    )
    argparser.add_argument(
        '--sl-path',
        type=str,
        help="Directory of the SL model",
    )
    argparser.add_argument(
        '--drl-path',
        type=str,
        help="Directory of the DRL model",
    )
    argparser.add_argument(
        '--num-samples',
        type=int,
        help="Number of interactions with the test environment",
        default=100,
    )

    args = argparser.parse_args()
    num_samples = args.num_samples
    drl_path = args.drl_path if args.drl_path.endswith('/') else args.drl_path + '/'
    sl_path = args.sl_path if args.sl_path.endswith('/') else args.sl_path + '/'
    global DATA_PATH
    if args.data_path:
        DATA_PATH =  (args.data_path if args.data_path.endswith('/') 
                      else args.data_path + '/')
    else:
        # Get data path from meta data file in SL path
        with open(sl_path + "meta_data.json", "r") as f:
            DATA_PATH = json.load(f)['data_path']
    global DATA_PATH_RESULTS 
    DATA_PATH_RESULTS = args.results_path if args.results_path.endswith('/') else args.results_path + '/'
    DATA_PATH_RESULTS += str(datetime.now().isoformat(timespec='seconds'))
    DATA_PATH_RESULTS += ('_' + args.description + '/') if args.description else '/'

    os.makedirs(os.path.dirname(DATA_PATH_RESULTS), exist_ok=True)

    env_name = env_name_from_json(DATA_PATH + "meta_data.json")
    env = getattr(envs, env_name)()
    env = gym.wrappers.RescaleAction(env, -1, 1)

    generate_data(env, num_samples, sl_path, drl_path)

    with open(DATA_PATH_RESULTS + "results.txt", "w") as f:
        f.write(f"Data path: {DATA_PATH}\n")
        f.write(f"SL path: {sl_path}\n")
        f.write(f"DRL path: {drl_path}\n")
        f.write(f"{num_samples} Samples\n")
        f.write(f"Device: {DEVICE}\n\n")

    # TODO: This re-loading is unnecessary!
    actions_opf = np.genfromtxt(DATA_PATH_RESULTS + "actions_opf.csv", delimiter=',')
    actions_sl = np.genfromtxt(DATA_PATH_RESULTS + "actions_sl.csv", delimiter=',')
    actions_drl = np.genfromtxt(DATA_PATH_RESULTS + "actions_drl.csv", delimiter=',')
    rewards_opf = np.genfromtxt(DATA_PATH_RESULTS + "rewards_opf.csv", delimiter=',')
    rewards_sl = np.genfromtxt(DATA_PATH_RESULTS + "rewards_sl.csv", delimiter=',')
    rewards_drl = np.genfromtxt(DATA_PATH_RESULTS + "rewards_drl.csv", delimiter=',')
    penalties_opf = np.genfromtxt(DATA_PATH_RESULTS + "penalties_opf.csv", delimiter=',')
    penalties_sl = np.genfromtxt(DATA_PATH_RESULTS + "penalties_sl.csv", delimiter=',')
    penalties_drl = np.genfromtxt(DATA_PATH_RESULTS + "penalties_drl.csv", delimiter=',')
    valids_opf = np.genfromtxt(DATA_PATH_RESULTS + "valids_opf.csv", delimiter=',')
    valids_sl = np.genfromtxt(DATA_PATH_RESULTS + "valids_sl.csv", delimiter=',')
    valids_drl = np.genfromtxt(DATA_PATH_RESULTS + "valids_drl.csv", delimiter=',')
    times_opf = np.genfromtxt(DATA_PATH_RESULTS + "times_opf.csv", delimiter=',')
    times_sl = np.genfromtxt(DATA_PATH_RESULTS + "times_sl.csv", delimiter=',')
    times_drl = np.genfromtxt(DATA_PATH_RESULTS + "times_drl.csv", delimiter=',')
    full_times_sl = np.genfromtxt(DATA_PATH_RESULTS + "full_times_sl.csv", delimiter=',')
    full_times_drl = np.genfromtxt(DATA_PATH_RESULTS + "full_times_drl.csv", delimiter=',')
    with open(DATA_PATH_RESULTS + "batch_times.json", "r") as f:
        batch_times = json.load(f)

    # EcoDispatchEnv: scale actions to prevent absurdly high mapes
    if (isinstance(env, EcoDispatchEnv)):
        scaler = ScalerObs(env.action_space.low, env.action_space.high)
        actions_opf = scaler(actions_opf)
        actions_drl = scaler(actions_drl)
        actions_sl = scaler(actions_sl)

    # Numpy arrays to boolschear arrays
    valids_sl = valids_sl.astype(bool)
    valids_drl = valids_drl.astype(bool)

    penalty_metrics(penalties_sl, penalties_drl,
                    rewards_sl[valids_sl], rewards_drl[valids_drl])
    actions_metrics(actions_opf, actions_sl, actions_drl)
    rewards_metrics(rewards_opf, rewards_sl, rewards_drl, valids_sl, valids_drl)
    computation_times_mean(times_opf, full_times_sl, full_times_drl, times_sl, 
                           times_drl, batch_times['sl'], batch_times['drl'])

    # Read computation times from all the different files
    with open(DATA_PATH + "meta_data.json", "r") as f:
        meta_data = json.load(f)
    training_sl_datageneration = meta_data["execution_time"] / 3600

    with open(sl_path + "results.json", "r") as f:
        results = json.load(f)    
    training_sl = results["execution_time"] / 3600

    drl_df = pd.read_csv(drl_path + "test_returns.csv")
    training_drl_test = drl_df["test_time"].values[-1] / 3600
    training_drl_env = drl_df["env_time"].values[-1] / 3600
    training_drl = drl_df['train_time'].values[-1] / 3600

    with open(DATA_PATH_RESULTS + "results.txt", "a") as f:
        f.write(f"Training time in h\n")
        f.write(f"SL: {training_sl}\n")
        f.write(f"SL Dataset generation: {training_sl_datageneration}\n")
        f.write(f"DRL: {training_drl}\n")
        f.write(f"DRL Env: {training_drl_env}\n")
        f.write(f"DRL Test: {training_drl_test}\n\n")
    training_time_plot(training_sl, training_sl_datageneration,
                       training_drl, training_drl_env, training_drl_test)


@torch.no_grad()
def generate_data(env, num_samples, sl_path, drl_path):
    # Initialize SL Model
    hyperparams = load_hyperparams(sl_path)
    sl_model = initialize_model(env, hyperparams)
    sl_model.load_state_dict(torch.load(sl_path + 'model.pt'))
    sl_model = sl_model.to(torch.float)

    # Initialize DRL Model
    drl_model = copy.deepcopy(sl_model)
    drl_model.load_state_dict(torch.load(drl_path + 'actor.pth'))
    drl_model = drl_model.to(torch.float)

    sl_model.eval()
    drl_model.eval()

    inputs = []
    actions_opf = []
    actions_sl = []
    actions_drl = []
    rewards_opf = []
    rewards_sl = []
    rewards_drl = []
    objectives_opf = []
    objectives_sl = []
    objectives_drl = []
    penalties_opf = []
    penalties_sl = []
    penalties_drl = []
    valids_opf = []
    valids_sl = []
    valids_drl = []
    times_opf = []
    times_sl = []
    times_drl = []
    full_times_sl = []
    full_times_drl = []

    scaler = ScalerObs(env.observation_space.low, env.observation_space.high)


    fail_counter = 0
    index = 0
    while index < num_samples:
        print(f'{index}/{num_samples}')

        # Create a random environment setting
        input, info = env.reset(options={'test': True})
        input = scaler(input)

        start = time()
        success = env.unwrapped._optimal_power_flow()
        execution_time = time() - start
        if not success:
            fail_counter += 1
            continue
        times_opf.append(execution_time)
        inputs.append(input)
        input = toTensor(input)

        # OPF
        action = env.unwrapped.get_current_actions()
        _, reward, _, _, _ = env.unwrapped.step(action)
        valids, viol, perc_viol, penalty = env.unwrapped.calc_violations()
        obj = sum(env.unwrapped.calc_objective(env.unwrapped.net))

        actions_opf.append(action)
        rewards_opf.append(reward)
        objectives_opf.append(obj)
        penalties_opf.append(penalty)
        valids_opf.append(valids.all())

        # SL
        start = time()
        action = toNumpy(sl_model(input.float()))
        # Measure inference time
        times_sl.append(time() - start)
        _, reward, _, _, _ = env.step(action)
        # Measure inference time + power flow calculation time
        # (TODO: Maybe some error because of other computations in step()?)
        full_times_sl.append(time() - start)
        valids, viol, perc_viol, penalty = env.unwrapped.calc_violations()
        obj = sum(env.unwrapped.calc_objective(env.unwrapped.net))

        actions_sl.append(action)
        rewards_sl.append(reward)
        objectives_sl.append(obj)
        penalties_sl.append(penalty)
        valids_sl.append(valids.all())

        # DRL
        start = time()
        action = toNumpy(drl_model(input.float()))
        times_drl.append(time() - start)
        _, reward, _, _, _ = env.step(action)
        full_times_drl.append(time() - start)
        valids, viol, perc_viol, penalty = env.unwrapped.calc_violations()
        obj = sum(env.unwrapped.calc_objective(env.unwrapped.net))

        actions_drl.append(action)
        rewards_drl.append(reward)
        objectives_drl.append(obj)
        penalties_drl.append(penalty)
        valids_drl.append(valids.all())

        index += 1

    # Compute batch inference time of SL and DRL (not possible for conv. OPF)
    batch_inputs = toTensor(inputs).float()
    start = time()
    batch_actions_sl = sl_model(batch_inputs)
    batch_time_sl = time() - start

    start = time()
    batch_actions_drl = drl_model(batch_inputs)
    batch_time_drl = time() - start

    np.savetxt(DATA_PATH_RESULTS + "inputs.csv", inputs, delimiter=',')

    np.savetxt(DATA_PATH_RESULTS + "actions_opf.csv", actions_opf, delimiter=',')
    np.savetxt(DATA_PATH_RESULTS + "actions_sl.csv", actions_sl, delimiter=',')
    np.savetxt(DATA_PATH_RESULTS + "actions_drl.csv", actions_drl, delimiter=',')

    np.savetxt(DATA_PATH_RESULTS + "rewards_opf.csv", rewards_opf, delimiter=',')
    np.savetxt(DATA_PATH_RESULTS + "rewards_sl.csv", rewards_sl, delimiter=',')
    np.savetxt(DATA_PATH_RESULTS + "rewards_drl.csv", rewards_drl, delimiter=',')

    np.savetxt(DATA_PATH_RESULTS + "penalties_opf.csv", penalties_opf, delimiter=',')
    np.savetxt(DATA_PATH_RESULTS + "penalties_sl.csv", penalties_sl, delimiter=',')
    np.savetxt(DATA_PATH_RESULTS + "penalties_drl.csv", penalties_drl, delimiter=',')

    np.savetxt(DATA_PATH_RESULTS + "valids_opf.csv", valids_opf, delimiter=',')
    np.savetxt(DATA_PATH_RESULTS + "valids_sl.csv", valids_sl, delimiter=',')
    np.savetxt(DATA_PATH_RESULTS + "valids_drl.csv", valids_drl, delimiter=',')

    np.savetxt(DATA_PATH_RESULTS + "times_opf.csv", times_opf, delimiter=',')
    np.savetxt(DATA_PATH_RESULTS + "times_sl.csv", times_sl, delimiter=',')
    np.savetxt(DATA_PATH_RESULTS + "times_drl.csv", times_drl, delimiter=',')

    np.savetxt(DATA_PATH_RESULTS + "full_times_sl.csv", full_times_sl, delimiter=',')
    np.savetxt(DATA_PATH_RESULTS + "full_times_drl.csv", full_times_drl, delimiter=',')

    # Write batch inference time to JSON
    batch_times = {"sl": batch_time_sl, "drl": batch_time_drl}
    with open(DATA_PATH_RESULTS + "batch_times.json", "w") as f:
        json.dump(batch_times, f, indent=4)

    print(f"OPF failure in %: {fail_counter/(fail_counter+index)*100}\n\n") 


def load_hyperparams(path):
    """ Load hyperparameter dict from JSON file. TODO: Move to somewhere else! """
    with open(path + "hyperparams.json", "r") as f:
        hyperparams = json.load(f)

    return hyperparams


# Calculate metrics

def penalty_metrics(penalties_sl, penalties_drl, rewards_sl_valid, rewards_drl_valid):
    sum_penalties_sl = np.mean(penalties_sl)
    sum_penalties_drl = np.mean(penalties_drl)

    invalid_share_sl = (len(penalties_sl) - len(rewards_sl_valid)) / \
        len(penalties_sl)
    invalid_share_drl = (len(penalties_drl) -
                    len(rewards_drl_valid)) / len(penalties_drl)

    penalty_plot(sum_penalties_sl, sum_penalties_drl)
    invalids_plot(invalid_share_sl, invalid_share_drl)

    with open(DATA_PATH_RESULTS + "results.txt", "a") as f:
        f.write(f"Penalties\n")
        f.write(f"SL: {sum_penalties_sl}\n")
        f.write(f"DRL: {sum_penalties_drl}\n\n")

        f.write(f"% invalid solutions\n")
        f.write(f"SL: {invalid_share_sl}\n")
        f.write(f"DRL: {invalid_share_drl}\n\n")


def rewards_metrics(rewards_opf, rewards_sl, rewards_drl, valids_sl, valids_drl):
    # filter out rewards that have any penalties
    rewards_sl_valid = rewards_sl[valids_sl]
    rewards_drl_valid = rewards_drl[valids_drl]
    rewards_opf_valid_sl = rewards_opf[valids_sl]
    rewards_opf_valid_drl = rewards_opf[valids_drl]

    rmse_sl = rmse(rewards_sl_valid, rewards_opf_valid_sl)
    rmse_drl = rmse(rewards_drl_valid, rewards_opf_valid_drl)

    mape_sl = mape(rewards_sl_valid, rewards_opf_valid_sl)
    mape_drl = mape(rewards_drl_valid, rewards_opf_valid_drl)

    rewards_rmse_plot(rmse_sl, rmse_drl)
    rewards_mape_plot(mape_sl, mape_drl)

    shared_valid = np.logical_and(valids_drl, valids_sl)

    # Plot all results (valid and invalid)
    rewards_boxplot(rewards_opf, rewards_sl, rewards_drl, "rewards_boxplot_all")
    # Compare only valid ML solutions to all OPF solutions
    rewards_boxplot(
        rewards_opf, rewards_sl[valids_sl], rewards_drl[valids_drl], "rewards_boxplot_valid_all_opf")
    # Commpare only shared valid solutions of DRL, SL, and OPF
    rewards_boxplot(rewards_opf[shared_valid], rewards_sl[shared_valid],
                    rewards_drl[shared_valid], "rewards_boxplot_shared_valid")

    with open(DATA_PATH_RESULTS + "results.txt", "a") as f:
        f.write(f"Valid RMSE to optimal rewards\n")
        f.write(f"SL: {rmse_sl}\n")
        f.write(f"DRL: {rmse_drl}\n\n")

        f.write(f"Valid MAPE to optimal rewards\n")
        f.write(f"SL: {mape_sl}\n")
        f.write(f"DRL: {mape_drl}\n\n")

        f.write(f"Mean rewards (shared valid)\n")
        f.write(f"SL: {np.mean(rewards_sl[shared_valid])}\n")
        f.write(f"DRL: {np.mean(rewards_drl[shared_valid])}\n")
        f.write(f"OPF: {np.mean(rewards_opf[shared_valid])}\n\n")


def actions_metrics(actions_opf, actions_sl, actions_drl):
    rmse_sl = rmse(actions_sl, actions_opf)
    rmse_drl = rmse(actions_drl, actions_opf)

    mape_sl = mape(actions_sl, actions_opf)
    mape_drl = mape(actions_drl, actions_opf)

    actions_mape_plot(mape_sl, mape_drl)
    actions_rmse_plot(rmse_sl, rmse_drl)

    with open(DATA_PATH_RESULTS + "results.txt", "a") as f:
        f.write(f"RMSE to optimal actions\n")
        f.write(f"SL: {rmse_sl}\n")
        f.write(f"DRL: {rmse_drl}\n\n")

        f.write(f"MAPE to optimal actions\n")
        f.write(f"SL: {mape_sl}\n")
        f.write(f"DRL: {mape_drl}\n\n")


def computation_times_mean(times_opf, full_times_sl, full_times_drl, times_sl, 
                           times_drl, batch_time_sl, batch_time_drl):
    time_opf = np.mean(times_opf)
    time_sl = np.mean(times_sl)
    time_drl = np.mean(times_drl)
    full_time_sl = np.mean(full_times_sl)
    full_time_drl = np.mean(full_times_drl)
    batch_time_sl = batch_time_sl / len(times_opf)
    batch_time_drl = batch_time_drl / len(times_opf)

    computation_times_mean_plot(time_opf, full_time_sl, full_time_drl, time_sl, 
                                time_drl, batch_time_sl, batch_time_drl)

    with open(DATA_PATH_RESULTS + "results.txt", "a") as f:
        f.write(f"Mean computation time\n")
        f.write(f"OPF: {time_opf}\n")
        f.write(f"SL: {time_sl}\n")
        f.write(f"DRL: {time_drl}\n")
        f.write(f"SL (batch): {batch_time_sl}\n")
        f.write(f"DRL (batch): {batch_time_drl}\n\n")


# Plot the metrics

def actions_mape_plot(mape_sl, mape_drl):
    labels = ["SL", "RL"]
    colors = ["C0", "C1"]
    data = [mape_sl, mape_drl]

    fig, ax = plt.subplots(figsize=FIG_SIZE)
    plt.bar(labels, data, color=colors)
    # ax.set_xlabel('Learning Type')
    ax.set_ylabel('MAPE in %')
    plt.rc('font', size=FONT_SIZE)
    # plt.title('MAPE zu den optimalen Aktionen')
    plt.savefig(DATA_PATH_RESULTS + "actions_mape.png")
    plt.close()


def actions_rmse_plot(rmse_sl, rmse_drl):
    labels = ["SL", "RL"]
    colors = ["C0", "C1"]
    data = [rmse_sl, rmse_drl]

    fig, ax = plt.subplots(figsize=FIG_SIZE)
    plt.bar(labels, data, color=colors)
    # ax.set_xlabel('Learning Type')
    ax.set_ylabel('RMSE')
    plt.rc('font', size=FONT_SIZE)
    # plt.title('RMSE zu den optimalen Aktionen')
    plt.savefig(DATA_PATH_RESULTS + "actions_rmse.png")
    plt.close()


def rewards_rmse_plot(rmse_sl, rmse_drl):
    labels = ["SL", "RL"]
    colors = ["C0", "C1"]
    data = [rmse_sl, rmse_drl]

    fig, ax = plt.subplots(figsize=FIG_SIZE)
    plt.bar(labels, data, color=colors)
    ax.set_ylabel('RMSE')
    plt.rc('font', size=FONT_SIZE)
    plt.savefig(DATA_PATH_RESULTS + "rewards_rmse.png")
    plt.close()


def rewards_mape_plot(mape_sl, mape_drl):
    labels = ["SL", "RL"]
    colors = ["C0", "C1"]
    data = [mape_sl, mape_drl]

    fig, ax = plt.subplots(figsize=FIG_SIZE)
    plt.bar(labels, data, color=colors)
    ax.set_ylabel('MAPE in %')
    plt.rc('font', size=FONT_SIZE)
    plt.savefig(DATA_PATH_RESULTS + "rewards_mape.png")
    plt.close()


def computation_times_mean_plot(time_opf, full_time_sl, full_time_drl, time_sl, 
                                time_drl, batch_time_sl, batch_time_drl):
    labels = ["OPF", "SL + PF", "RL + PF", "SL", "RL", "SL (batch)", "RL (batch)"]
    colors = ["C2", "C0", "C1", "C0", "C1", "C0", "C1"]
    data = [time_opf, full_time_sl, full_time_drl, time_sl, time_drl, batch_time_sl, batch_time_drl]

    fig, ax = plt.subplots(figsize=FIG_SIZE)
    plt.bar(labels, data, color=colors)
    plt.yscale("log")
    ax.set_ylabel('Time in s')
    plt.rc('font', size=FONT_SIZE)
    plt.savefig(DATA_PATH_RESULTS + "computation_time.png")
    plt.close()


def training_time_plot(training_sl, training_sl_datageneration, training_drl, 
                       training_drl_env, training_drl_test):
    labels = ["SL", "RL"]
    valuesTop = [training_sl, training_drl]
    valuesBottom = [training_sl_datageneration, 0]
    valuesBottom2 = [0,0]  # [0, training_drl_test]
    valuesBottom3 = [0, training_drl_env]

    fig, ax = plt.subplots(figsize=FIG_SIZE)
    plt.bar(labels, valuesBottom, color=["C2"], label="Dataset generation")
    # plt.bar(labels, valuesBottom2, color=["C1"], label="Testing")
    plt.bar(labels, valuesBottom3, bottom=valuesBottom2, color=["C1"], label="Environment")
    bottom=[x+y+z for x, y, z in zip(valuesBottom, valuesBottom2, valuesBottom3)]
    plt.bar(labels, valuesTop, bottom=bottom, color=["C0"], label="Training")
    ax.set_ylabel('Time in h')
    plt.rc('font', size=FONT_SIZE)
    plt.legend()
    plt.savefig(DATA_PATH_RESULTS + "training_time.png")
    plt.close()


def rewards_boxplot(rewards_opf, rewards_sl, rewards_drl, name):
    labels = ["OPF", "SL", "RL"]
    colors = ["C2", "C0", "C1"]
    data = [rewards_opf, rewards_sl, rewards_drl]

    fig, ax = plt.subplots(figsize=(10, 7))
    boxprops = dict(linestyle='-', linewidth=2, color='black')
    flierprops = dict(marker='o', markerfacecolor='black', markersize=5,
                      linestyle='none')
    capprops = dict(linewidth=2, color='black')
    medianprops = dict(linestyle='-', linewidth=2, color='black')

    # Change the color of the boxplots
    bplots = ax.boxplot(data, labels=labels, patch_artist=True, showfliers=False, showmeans=True, meanline=True,
                        meanprops={"linestyle": "-", "linewidth": 2, "color": "red"}, widths=0.15,
                        positions=[1, 1.5, 2])
    for patch, color in zip(bplots['boxes'], colors):
        patch.set_facecolor(color)

    # Change the color of the median line
    for median in bplots['medians']:
        median.set(**medianprops)

    # Create dummy lines with desired styles and colors
    mean_line = plt.Line2D([], [], linestyle='-', linewidth=2, color='red')
    median_line = plt.Line2D([], [], linestyle='-', linewidth=2, color='black')

    # Add legend with mean and median lines
    plt.legend([mean_line, median_line], ['Mean', 'Median'], loc='upper right')

    ax.set_ylabel('Reward')
    plt.rc('axes', titlesize=FONT_SIZE)  # fontsize of the axes title
    plt.rc('axes', labelsize=FONT_SIZE)
    plt.savefig(DATA_PATH_RESULTS + name + ".png")
    plt.close()

    rewards_boxplot_fliers(rewards_opf, rewards_sl, rewards_drl, name)


def rewards_boxplot_fliers(rewards_opf, rewards_sl, rewards_drl, name):
    labels = ["OPF", "SL", "RL"]
    colors = ["C2", "C0", "C1"]
    data = [rewards_opf, rewards_sl, rewards_drl]

    fig, ax = plt.subplots(figsize=(10, 7))
    bplots = ax.boxplot(data, labels=labels, patch_artist=True,
                        showfliers=True, widths=0.15, positions=[1, 1.5, 2])

    for patch, color in zip(bplots['boxes'], colors):
        patch.set_facecolor(color)

    ax.set_ylabel('Reward')
    plt.rc('font', size=FONT_SIZE)
    plt.savefig(DATA_PATH_RESULTS + name + "_fliers.png")
    plt.close()


def penalty_plot(sum_penalties_sl, sum_penalties_drl):
    labels = ["SL", "RL"]
    colors = ["C0", "C1"]
    data = [sum_penalties_sl, sum_penalties_drl]

    fig, ax = plt.subplots(figsize=FIG_SIZE)
    plt.bar(labels, data, color=colors)
    ax.set_ylabel('Penalty')
    plt.rc('font', size=FONT_SIZE)
    plt.savefig(DATA_PATH_RESULTS + "penalties.png")
    plt.close()


def invalids_plot(invalids_sl, invalids_drl):
    labels = ["SL", "RL"]
    colors = ["C0", "C1"]
    data = [invalids_sl * 100, invalids_drl * 100]

    fig, ax = plt.subplots(figsize=FIG_SIZE)
    plt.bar(labels, data, color=colors)
    ax.set_ylabel('Share of invalid solutions in %')
    plt.rc('font', size=FONT_SIZE)
    plt.savefig(DATA_PATH_RESULTS + "invalid_solutions.png")
    plt.close()


if __name__ == '__main__':
    main()