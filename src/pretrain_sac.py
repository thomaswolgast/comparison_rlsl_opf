
import numpy as np
import torch
from drl.sac import Sac

from pretrain_ddpg import PretrainOffPolicyMixin


class PretrainSac(Sac, PretrainOffPolicyMixin):
    def __init__(self, env, delay_alpha_update=False, 
                 entropy_while_supervised=False,
                 *args, **kwargs):
        super().__init__(env, *args, **kwargs)

        self.delay_alpha_update = delay_alpha_update
        self.entropy_while_supervised = entropy_while_supervised
        self.init_pretrain(*args, **kwargs)

    @torch.no_grad()
    def act(self, obs):
        """ Use actor to create actions and add noise for exploration. """
        if (len(self.memory) < (self.start_train + self.actor_training_delay) 
                and self.random_init_actions):
            return self.env.action_space.sample()

        obs = torch.tensor(obs, dtype=torch.float).to(self.device)
        return np.clip(self.actor(obs, act_only=True).cpu().numpy(), -1, 1)

    def _learn(self, *args, **kwargs):
        if self.retrain_interval and self.step % self.retrain_interval == 0:
            self.supervised_learning_step(*next(iter(self.opt_train_data)))
        super()._learn(*args, **kwargs)

    def _train_actor(self, obss, acts, rewards, next_obss, dones):
        # Delay actor training to prevent overwriting supervised learning
        # with still badly trained critic gradients
        
        self.actor.optimizer.zero_grad()
        entropy, acts = self.actor.forward(obss)
        if len(self.memory) > (self.start_train + self.actor_training_delay):
            # Train actor with critic loss + entropy
            q_values = torch.minimum(self.critic1(obss, acts).sum(axis=1),
                                     self.critic2(obss, acts).sum(axis=1))

            actor_loss = -(q_values + self.alpha.detach() * entropy).mean()
        elif not self.delay_alpha_update:
            # Train only entropy because critic is not trained yet
            # TODO Problem: This will change the deterministic policy as well!
            # Approach: Perform supervised learning and entropy learning together?
            # Alternative: Separate networks for deterministic and stochastic policy? 
            actor_loss = torch.nn.functional.mse_loss(acts, acts)  # Does this work to prevent changing policy?
            actor_loss += -(self.alpha.detach() * entropy).mean()
            print('entropy: ' , entropy.detach().mean())
            print('alpha: ', self.alpha.detach())
            print('acts: ', acts.detach().numpy()[1,:])
        else: 
            return

        actor_loss.backward()
        if self.grad_clip is not None:
            torch.nn.utils.clip_grad_norm_(
                self.actor.parameters(), self.grad_clip)
        self.actor.optimizer.step()

    def _update_alpha(self, obss):
        # TODO: Delay as well? Probably not useful since we want to explore!
        if not self.delay_alpha_update:
            super()._update_alpha(obss)
        elif len(self.memory) > (self.start_train + self.actor_training_delay):
            super()._update_alpha(obss)

    def supervised_learning_step(self, obs, opt_acts):
        """ Overwrite supervised learning from Mixin because special SAC model"""
        self.model.train()
        self.pretrain_optimizer.zero_grad()
        entropy, act = self.model.forward(obs)
        loss = torch.nn.functional.mse_loss(act, opt_acts)
        if self.entropy_while_supervised:
            # Train entropy as well
            loss += -(self.alpha.detach() * entropy).mean()

        loss.backward()
        self.pretrain_optimizer.step()

    def validation_test(self):
        """ Same as supervised_learning_step(). """
        self.model.eval()
        obs, opt_act = next(iter(self.opt_val_data))
        act = self.model(obs, act_only=True, deterministic=True)
        loss = (act - opt_act).abs().mean()
        return loss.item()
