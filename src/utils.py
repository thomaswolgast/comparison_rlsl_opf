import json

import numpy as np
import torch

DEVICE = ('cuda:0' if torch.cuda.is_available() else 'cpu')


def toNumpy(tensor):
    if isinstance(tensor, np.ndarray):
        return tensor
    else:
        return tensor.detach().cpu().numpy()


def toTensor(ndarray):
    if isinstance(ndarray, torch.Tensor):
        return ndarray.to(DEVICE)
    else:
        return torch.tensor(ndarray).to(DEVICE)


def mape(outputs, targets):
    outputs = toNumpy(outputs)
    targets = toNumpy(targets)
    return np.mean(np.abs((targets - outputs) / targets)) * 100


def rmse(outputs, targets):
    if outputs.ndim == 1:
        return np.sqrt(np.square(targets - outputs).mean())
    elif outputs.ndim == 2:
        error = np.zeros(outputs.shape[0])
        for i in range(outputs.shape[0]):
            error[i] = np.sqrt(np.square(targets[i, :] - outputs[i, :]).mean())
        return np.mean(error)
    else:
        raise NotImplementedError
    

def env_name_from_json(path):
    """ Get the environment name from a JSON file. """
    with open(path, 'r') as f:
        data = json.load(f)
    return data['environment']


def rescale_action(act, action_space, target_action_space):
    """ Rescale action from a given action space to a target action space. """
    return ((target_action_space.high - target_action_space.low) *
            (act - action_space.low) / (action_space.high - action_space.low) 
            + target_action_space.low)
