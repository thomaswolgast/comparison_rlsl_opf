import argparse
import json
import os
from time import time

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.utils.data.dataset as dataset
from torch.utils.data import DataLoader

from drl.networks import DDPGActorNet
import mlopf.envs as envs

from utils import mape, env_name_from_json

DEVICE = ('cuda:0' if torch.cuda.is_available() else 'cpu')


def main():
    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        '--data-path',
        type=str,
        help="Directory of the data folder",
    )
    argparser.add_argument(
        '--description',
        type=str,
        default="",
        help="Short description of the experiment.",
    )
    argparser.add_argument(
        '--hyperparams',
        type=str,
        help="Hyperparameters in dict form.",
        default="{}"
    )
    argparser.add_argument(
        '--train-share',
        type=float,
        help="Share of the training data (validation share: 1 - train_share)",
        default=0.8,
    )
    args = argparser.parse_args()

    data_path = args.data_path if args.data_path.endswith('/') else args.data_path + '/'
    description = '_' + args.description if args.description else ''
    path = f"supervised_learning/{data_path.split('/')[-2]}{description}/"
    hyperparams = {'batch_size': 8,
                   'fc_dims': (256, 256, 256),
                   'learning_rate': 0.001,
                   'epochs': 50,
                   'optimizer': 'Adam',
                   'output_activation': 'tanh'}
    hyperparams.update(eval(args.hyperparams))

    # Read environment class name from meta_data.json
    env_name = env_name_from_json(data_path + "meta_data.json")
    env = getattr(envs, env_name)()

    # Prepare and split the dataset and create dataloaders
    train_dl, val_dl = prepare_data(
        data_path, hyperparams['batch_size'], args.train_share)

    # Initialize Model
    model = initialize_model(env, hyperparams)

    # Store some meta information
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path + "meta_data.json", "w") as f:
        meta = {"env_name": env_name,
                "description": args.description,
                "data_path": data_path,
                "Train/Validation split": args.train_share}
        json.dump(meta, f, indent=4)

    # Store hyperparameter dict as JSON
    with open(path + "hyperparams.json", "w") as f:
        json.dump(hyperparams, f)

    # Train the model
    train_model(train_dl, val_dl, model, env, path, hyperparams['epochs'])


def prepare_data(data_path, batch_size, train_share=0.8):
    print("Preparing data...")

    inputs = np.genfromtxt(data_path + "inputs.csv", delimiter=',')
    targets = np.genfromtxt(data_path + "targets.csv", delimiter=',')

    data = Dataset(inputs, targets)

    # Split data into train and validation sets
    train_size = int(train_share * len(data))
    valid_size = len(data) - train_size

    print(f"Train size: {train_size} | Val size: {valid_size}")

    train, valid = dataset.random_split(data, [train_size, valid_size])

    train_dl = DataLoader(train, batch_size=batch_size)
    valid_dl = DataLoader(valid, batch_size=batch_size)

    return train_dl, valid_dl


class Dataset(dataset.Dataset):
    def __init__(self, inputs, targets):
        super().__init__()
        self.inputs = inputs
        self.targets = targets

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, idx):
        return self.inputs[idx, :], self.targets[idx, :]


def initialize_model(env, hyperparams):
    n_obs = env.observation_space.shape[0]
    n_act = env.action_space.shape[0]

    model = DDPGActorNet(n_obs=n_obs, n_act=n_act, **hyperparams)
    model.to(DEVICE)

    return model


def train_model(train_dl: DataLoader, valid_dl: DataLoader,
                model: DDPGActorNet, env, path, epochs):

    print("Starting Training...")

    train_losses = []
    train_mape = []
    valid_losses = []
    valid_mape = []
    best_valid_mape = float('inf')
    criterion = nn.MSELoss()

    start = time()
    for epoch in range(epochs):
        print("--------------------------------------")
        print("Epoch: " + str(epoch + 1) + "/" + str(epochs))

        train_epoch_loss = []
        train_epoch_mape = []
        valid_epoch_loss = []
        valid_epoch_mape = []

        model.train()
        for i, (inputs, targets) in enumerate(train_dl):
            inputs = inputs.float().to(DEVICE)
            targets = targets.float().to(DEVICE)
            model.optimizer.zero_grad()

            outputs = model(inputs)

            loss = criterion(outputs, targets)
            train_epoch_loss.append(loss.item())
            train_epoch_mape.append(mape(outputs, targets))

            loss.backward()
            model.optimizer.step()

        train_losses.append(np.mean(train_epoch_loss))
        train_mape.append(np.mean(train_epoch_mape))

        # Validate
        model.eval()
        with torch.no_grad():
            for i, (inputs, targets) in enumerate(valid_dl):
                inputs = inputs.float().to(DEVICE)
                targets = targets.float().to(DEVICE)

                outputs = model(inputs)

                loss = criterion(outputs, targets)
                valid_epoch_loss.append(loss.item())
                valid_epoch_mape.append(mape(outputs, targets))

        valid_losses.append(np.mean(valid_epoch_loss))
        valid_mape.append(np.mean(valid_epoch_mape))

        print(f"Train Loss: {train_losses[-1]:.3f}")
        print(f"Train Mape: {train_mape[-1]:.3f}")
        print(f"Validation Loss: {valid_losses[-1]:.3f}")
        print(f"Validation Mape: {valid_mape[-1]:.3f}")

        # save the best model if valid epoch mape decreases
        if best_valid_mape > valid_mape[-1]:
            best_valid_mape = valid_mape[-1]
            torch.save(model.state_dict(), path + "model.pt")

        # save plots
        save_plots(train_losses, train_mape, valid_losses, valid_mape, path)

    execution_time = time() - start

    np.save(path + "train_losses.npy", train_losses)
    np.save(path + "train_mape.npy", train_mape)
    np.save(path + "valid_losses.npy", valid_losses)
    np.save(path + "valid_mape.npy", valid_mape)

    results = {"train_losses": train_losses[-1],
               "train_mape": train_mape[-1],
               "valid_losses": valid_losses[-1],
               "valid_mape": valid_mape[-1],
               "execution_time": execution_time}
    with open(path + "results.json", "w") as f:
        json.dump(results, f, indent=4)


def save_plots(train_loss, train_mape, valid_loss, valid_mape, path):
    # MAPE plot
    plt.figure(figsize=(10, 7))
    plt.plot(train_mape, color='C0', linestyle='-', label='Train')
    plt.plot(valid_mape, color='C1', linestyle='-', label='Validation')
    plt.xlabel('Epochs')
    plt.ylabel('MAPE in %')
    plt.legend()
    plt.grid()
    plt.savefig(path + "training_mapes.png")
    plt.close()

    # loss plot
    plt.figure(figsize=(10, 7))
    plt.plot(train_loss, color='C0', linestyle='-', label='Train')
    plt.plot(valid_loss, color='C1', linestyle='-', label='Validation')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.grid()
    plt.savefig(path + "training_losses.png")
    plt.close()


if __name__ == '__main__':
    main()
